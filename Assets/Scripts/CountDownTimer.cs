﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using TMPro;
using System;

public class CountDownTimer : MonoBehaviour {
    private bool countDown = false;
    private float timeLeft = 30.0f;

    public UnityEvent countDownOver;

    [SerializeField]
    private TextMeshProUGUI timerText;

    private void Update()
    {
        if (countDown)
        {
            timeLeft -= Time.deltaTime;
            timerText.text = Mathf.RoundToInt(timeLeft).ToString();
            if (timeLeft < 0)
            {
                countDownOver.Invoke();
                countDown = false;
            }else if(timeLeft < 10)
            {
                HoopsGM.instance.MoveBackboard();
            }
        }
    }

    public void StartCountdown(float time)
    {
        timeLeft = time;
        countDown = true;
    }

    internal void AddTime(float v)
    {
        // TO DO : Show indicator that time was increased. Magical powder effect? //
        timeLeft += v;
    }
}
