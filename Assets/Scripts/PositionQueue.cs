﻿using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PositionQueue
{
    private Queue<Vector3> _queue;
    private int _maxSize;
    public int Count
    {
        get
        {
            return _queue.Count;
        }
    }
    public PositionQueue(int maxSize)
    {
        _maxSize = maxSize;
        _queue = new Queue<Vector3>();
    }
    public void Enqueue(Vector3 v)
    {
        if (_queue.Count >= _maxSize)
        {
            _queue.Dequeue();
        }
        _queue.Enqueue(v);
    }
    public Vector3 Peek()
    {
        return _queue.Peek();
    }
    public void Clear()
    {
        _queue.Clear();
    }

    public Vector3 GetMeanVector()
    {
        if (_queue.Count == 0)
            return Vector3.zero;
        float x = 0f;
        float y = 0f;
        float z = 0f;
        foreach (Vector3 pos in _queue.ToArray())
        {
            x += pos.x;
            y += pos.y;
            z += pos.z;
        }
        x = x / _queue.Count;
        y = y / _queue.Count;
        z = z / _queue.Count;

        //Debug.Log(y);
        return new Vector3(x, y, z);
    }
}