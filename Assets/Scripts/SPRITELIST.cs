﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class SPRITELIST : MonoBehaviour {
    public List<Texture2D> spriteSheets;

    public List<Sprite> sprites;

    public static SPRITELIST instance;

    void Awake()
    {
        MakeSingleton();
    }

    private void MakeSingleton()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);
    }

    public void LoadSpriteSheets()
    {
        foreach(Texture2D t in spriteSheets) 
        {
            sprites.AddRange(Resources.LoadAll<Sprite>("Sprites/" + t.name));
        }
    }

    internal Sprite GetSprite(string graphicName)
    {
        foreach(Sprite s in sprites)
        {
            if (s.name == graphicName)
            {
                return s;
            }
        }
        return null;
    }
}