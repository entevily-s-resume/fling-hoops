﻿using System;
using UnityEngine;

public class EarnACoin : MonoBehaviour {

    [SerializeField]
    private AudioClip contactNoise;
    [SerializeField]
    private Animator coinAnimator;

    private bool moving = false;
    private float speed = 200;
    private bool inAction = false;

    private int virtuallyCollect = 0;

    public bool TheCoinIsMoving()
    {
        return inAction;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "ball")
        {
            PlayAudio();
            Animate();
        }
    }

    public void Animate()
    {
        inAction = true;
        coinAnimator.SetBool("startMoving", true);
    }

    public void SpawnCoinBetween(Vector3 onePosition, Vector3 twoPosition)
    {
        Debug.Log("One position: " + onePosition + "two pos:" + twoPosition);
        float randomLR = UnityEngine.Random.Range(onePosition.x, twoPosition.x);
        float randomUD = UnityEngine.Random.Range(onePosition.y, twoPosition.y);
        transform.position = new Vector2(randomLR, randomUD);
    }

    private void IncrementCoins()
    {
        HoopsGM.instance.AddCoin(virtuallyCollect + 1, this);
        virtuallyCollect = 0;
    }

    public void MoveToLocation()
    {
        coinAnimator.SetBool("startMoving", false);
        moving = true;
    }

    void Update()
    {
        if (moving)
        {
            float step = speed * Time.deltaTime;
            transform.position = Vector3.MoveTowards(transform.position, GameObject.Find("OpenStore").transform.position, step);
            if(Vector2.Distance(transform.position,GameObject.Find("OpenStore").transform.position) < .5f)
            {
                moving = false;
                inAction = false;
                IncrementCoins();
            }
        }
    }

    internal void PlayAudio()
    {
        GetComponent<AudioSource>().PlayOneShot(contactNoise);
    }

    internal void VirtuallyCollect(int virtuallyCollect)
    {
        this.virtuallyCollect = virtuallyCollect;
    }
}
