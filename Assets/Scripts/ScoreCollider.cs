﻿using UnityEngine;

public class ScoreCollider : MonoBehaviour {
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "ball" && !other.GetComponent<Ball>().HasScored())
        {
            other.GetComponent<Ball>().Scored();
            if (other.GetComponent<Ball>().HitTheRim())
            {
                HoopsGM.instance.AddPoint();
            }
            else
            {
                HoopsGM.instance.AddPoint(2);
            }
        }
    }
}
