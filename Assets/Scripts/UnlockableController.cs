﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class UnlockableController : MonoBehaviour {
    public static UnlockableController instance;

    public List<UnlockableSegment> unlockableSegments;

    public GameObject unlockableItemPrefab;

    [SerializeField]
    public UnlockableItemsWrapper unlockableItems;

    private void Awake()
    {
        MakeSingleton();
    }

    private void MakeSingleton()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);
    }

    void Start()
    {
        LoadItemDatabase();
        SPRITELIST.instance.LoadSpriteSheets();
        LoadUI();
    }

    private void LoadUI()
    {
        foreach (UnlockableItem i in unlockableItems.list)
        {
            foreach(UnlockableSegment s in unlockableSegments)
            {
                if(s.segmentType == i.unlockableType)
                {
                    GameObject obj = Instantiate(unlockableItemPrefab, s.unlockableGroup.transform);
                    obj.GetComponent<ItemInfo>().SetInfo(i);
                }
            }
        }
    }

    void LoadItemDatabase()
    {
        string filePath = "DB/items";

        TextAsset targetFile = Resources.Load<TextAsset>(filePath);
        //Debug.LogError(targetFile.text);

        if (!string.IsNullOrEmpty(targetFile.text))
        {
            unlockableItems = JsonUtility.FromJson<UnlockableItemsWrapper>(targetFile.text);
        }
        else
        {
            unlockableItems = new UnlockableItemsWrapper();
        }
    }
}
