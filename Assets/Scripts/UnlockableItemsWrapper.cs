﻿using System.Collections.Generic;
using System;

[Serializable]
public class UnlockableItemsWrapper
{
    public List<UnlockableItem> list;
}