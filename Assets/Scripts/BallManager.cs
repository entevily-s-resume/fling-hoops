﻿using System;
using TMPro;
using UnityEngine;

public class BallManager : MonoBehaviour {
    public static BallManager instance;

    [SerializeField]
    private Transform left;
    [SerializeField]
    private Transform right;
    [SerializeField]
    private SpriteRenderer graphic;
    [SerializeField]
    private Camera mainCamera;
    [SerializeField]
    private Animator shadowAnim;
    [SerializeField]
    private float playerAccuracy;
    [SerializeField]
    private GameObject electricityGraphic;

    [SerializeField]
    private float leftMostPos, rightMostPos;

    public void SetPlayerAccuracy(float acs)
    {
        playerAccuracy = acs;
    }

    private PositionQueue pastPositions;
    private Vector3 velocity;
    private Vector3 startpos;
    private Vector3 endpos;

    private bool mouseDown = false;
    private bool clickable =true;
    private bool timesUp = false;
    private int timesUpMax = 10;
    private int timesUpCur = 0;

    void Awake()
    {
        pastPositions = new PositionQueue(500);
        instance = this;
    }

    internal void ChangePropertiesOfBallTo(UnlockableItem info)
    {
        graphic.sprite = SPRITELIST.instance.GetSprite(info.graphicName);
    }

    void FixedUpdate()
    {
        Vector3 touchPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Vector3 newPosition = new Vector3(touchPosition.x, touchPosition.y, transform.position.z);

        if (!HoopsGM.instance.ObstructedByUI())
        {
            GetComponent<CircleCollider2D>().enabled = true;
        }
        else
        {
            GetComponent<CircleCollider2D>().enabled = false;
        }

        if (HoopsGM.instance.flingable && !HoopsGM.instance.ObstructedByUI() && clickable && Input.GetMouseButtonDown(0))
        {
            RaycastHit2D hit = Physics2D.Raycast(new Vector2(mainCamera.ScreenToWorldPoint(Input.mousePosition).x, mainCamera.ScreenToWorldPoint(Input.mousePosition).y), Vector2.zero, 0f);
            if (hit.collider != null)
            {
                if (hit.transform.name == "BallManager")
                {
                    pastPositions.Clear();
                    startpos = newPosition;
                    mouseDown = true;
                }
            }
        }
        
        // User is dragging the object around
        if (Input.GetMouseButton(0))
        {
            if (mouseDown) { pastPositions.Enqueue(newPosition); }
            timesUpCur++;
            if (timesUpCur >= timesUpMax)
                timesUp = true;
        }

        if (Input.GetMouseButtonUp(0) || timesUp)
        {
            timesUp = false; timesUpCur = 0;
            if (mouseDown)
            {
                endpos = newPosition;
                velocity = (newPosition - pastPositions.Peek()) / playerAccuracy;// / pastPositions.Count;

                //Debug.Log(velocity.magnitude);

                if (velocity.y > 0 && velocity.magnitude >= .01)
                {
                    GameObject t = ObjectPooler.instance.SpawnFromPool("Ball", transform.position, transform.rotation);
                    Ball ball = t.GetComponent<Ball>();
                    Rigidbody2D ballRB = t.GetComponent<Rigidbody2D>();
                    ball.SetGraphic(graphic.sprite);

                    if (electricityGraphic.activeSelf)
                        ball.electricity.SetActive(true);
                    else
                        ball.electricity.SetActive(false);

                    ballRB.AddTorque(UnityEngine.Random.Range(-500f, 500f));
                    //velocity.z = 10;

                    if(HoopsGM.instance.gameMode == GAMEMODE.STANDARD)
                        Graphic(false);

                    velocity.y = 2.05f;
                    
                    t.GetComponent<Rigidbody2D>().velocity = velocity * 10f;
                    
                    //t.GetComponent<Ball>().StartingDistance = Vector3.Distance(GameObject.Find("Rim").transform.position, transform.position);
                    mouseDown = false;
                    if (!HoopsGM.instance.Started)
                        HoopsGM.instance.StartGame();
                }
            }
        }
    }

    public void RandomizeLocation(int score)
    {
        if(score >= 5)
        {
            Vector3 cur = transform.position;
            cur.x = UnityEngine.Random.Range(leftMostPos, rightMostPos);
            transform.position = cur;
        }
        else
        {
            Vector3 cur = transform.position;
            cur.x = 0;
            transform.position = cur;
        }
    }

    public void Graphic(bool g = true)
    {
        graphic.enabled = g;
        shadowAnim.SetBool("goingAway", !g);
        foreach(Transform o in electricityGraphic.transform)
        {
            o.gameObject.SetActive(g);
        }
        clickable = g;
    }

    public void Electrify(bool now = false)
    {
        if (now)
            electricityGraphic.SetActive(true);
        else
            electricityGraphic.SetActive(false);
    }
}