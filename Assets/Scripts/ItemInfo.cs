﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ItemInfo : MonoBehaviour {
    public UnlockableItem info;
    int price = 0;
    [SerializeField]
    private TextMeshProUGUI priceText;
    [SerializeField]
    private GameObject buttonContents;

    private Image itemImage; 

    void Awake()
    {
        itemImage = GetComponent<Image>();
    }

    internal void SetInfo(UnlockableItem i)
    {
        info.args = i.args;
        info.graphicName = i.graphicName;
        info.id = i.id;
        info.name = i.name;
        info.unlockableType = i.unlockableType;

        switch (info.unlockableType)
        {
            case SegmentType.BALL_COMMON:
                price = 100;
                break;
            case SegmentType.BALL_EXTINCT:
                price = 500;
                break;
            case SegmentType.BALL_RARE:
                price = 200;
                break;
            case SegmentType.BOARD_COMMON:
                price = 150;
                break;
            case SegmentType.BOARD_EXTINCT:
                price = 600;
                break;
            case SegmentType.BOARD_RARE:
                price = 300;
                break;
        }

        SetUpGraphics();
    }

    private void SetUpGraphics()
    {
        priceText.text = price.ToString();

        if (PlayerPrefs.HasKey(info.id) && PlayerPrefs.GetInt(info.id) == 1)
        {
            itemImage.sprite = SPRITELIST.instance.GetSprite(info.graphicName);
            buttonContents.SetActive(false);
        }
        else
        {
            buttonContents.SetActive(true);

            itemImage.sprite = SPRITELIST.instance.GetSprite("unknown");
        }
    }

    public void ItemClicked()
    {
        if (Unlocked(info.id))
        {
            BallManager.instance.ChangePropertiesOfBallTo(info);
            HoopsGM.instance.CloseOverlay();
        }
        else
        {
            if (HoopsGM.instance.HasEnoughCoins(price)) {
                HoopsGM.instance.SpendCoins(price);
                PlayerPrefs.SetInt(info.id, 1);
                SetUpGraphics();
            }
            else
            {
                HoopsGM.instance.OpenStore();
            }
        }
    }

    private bool Unlocked(string id)
    {
        if(PlayerPrefs.HasKey(id) && PlayerPrefs.GetInt(id) == 1)
        {
            return true;
        }
        return false;
    }
}
