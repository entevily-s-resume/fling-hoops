﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public enum GAMEMODE
{
    STANDARD,
    SHOOTOUT
}

public class HoopsGM : MonoBehaviour
{
    public static HoopsGM instance;

    public GAMEMODE gameMode;
    public bool debug = false;
    [SerializeField]
    private TextMeshProUGUI playingScoreText, bestScoreText, lastScoreText, coinText, coinText2, gameModeText;
    [SerializeField]
    private GameObject playingScore, nonPlayingScore, gameModeTextHolder;
    [SerializeField]
    [Range(0, 100)]
    private int coinChance;
    [SerializeField]
    private GameObject store, overlay, customize;
    [SerializeField]
    private AdBanner banner;
    [SerializeField]
    private BackboardController backboard;
    [SerializeField]
    private CountDownTimer countDownTimer;
    [SerializeField]
    private Button adsButton, freeMoneyButton;
    [SerializeField]
    private Transform dailyButton;
    [SerializeField]
    private Animator background;
    [SerializeField]
    private GameObject logo;

    private List<Ball> activeBalls = new List<Ball>();
    private List<EarnACoin> activeCoins = new List<EarnACoin>();

    private bool loggedIn = false;
    private bool started = false;
    private bool hasAds = true;

    private int score = 0;
    private int multiplyer = 1;
    private int coins = 0;
    private int perfectsInARow = 0;

    private float adTimer = 4;
    public bool flingable = true;

    public bool Started { get { return started; } }
    public bool HasAds
    {
        get { return hasAds; }
    }
    public int GetScore
    {
        get { return score; }
    }

    private void Awake()
    {
        MakeSingleton();
        // Check if we have a pref key saved for ads //
        if (PlayerPrefs.HasKey(ENT_CONSTANTS.PREFS_HAS_ADS))
            hasAds = PlayerPrefs.GetInt(ENT_CONSTANTS.PREFS_HAS_ADS) != 0;
    }

    private void MakeSingleton()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);
    }

    private void Start()
    {
        // Load In Coins //
        if (PlayerPrefs.HasKey(ENT_CONSTANTS.PREFS_TRACK_COINS))
            coins = PlayerPrefs.GetInt(ENT_CONSTANTS.PREFS_TRACK_COINS);
        
        DisableRemoveAdsButton();
        UpdateCoinVisually();
        LoadGameMode();
    }   

    /// <summary>
    /// Set the Remove Ads Button's interactivity based on HAS ADS player preference.
    /// </summary>
    private void DisableRemoveAdsButton()
    {
        adsButton.interactable = !hasAds;
    }

    /// <summary>
    /// Update both coin text fields to proper coin look/value.
    /// </summary>
    private void UpdateCoinVisually()
    {
        coinText.text = "x " + coins.ToString();
        coinText2.text = "x " + coins.ToString();
    }

    /// <summary>
    /// Hides/Shows UI elements and gameobjects based on the current gamemode that is set.
    /// </summary>
    public void LoadGameMode()
    {
        if (gameMode == GAMEMODE.SHOOTOUT)
        {
            countDownTimer.gameObject.SetActive(true);
            lastScoreText.text = PlayerPrefs.GetInt(ENT_CONSTANTS.PREFS_LAST_SCORE_SHOOTOUT).ToString();
            bestScoreText.text = PlayerPrefs.GetInt(ENT_CONSTANTS.PREFS_HIGH_SCORE_SHOOTOUT).ToString();
            gameModeText.text = ENT_CONSTANTS.STR_GM_SHOOTOUT;

        }
        else if (gameMode == GAMEMODE.STANDARD)
        {
            countDownTimer.gameObject.SetActive(false);
            gameModeText.text = ENT_CONSTANTS.STR_GM_STANDARD;
            lastScoreText.text = PlayerPrefs.GetInt(ENT_CONSTANTS.PREFS_LAST_SCORE_STANDARD).ToString();
            bestScoreText.text = PlayerPrefs.GetInt(ENT_CONSTANTS.PREFS_HIGH_SCORE_STANDARD).ToString();
        }
    }

    /// <summary>
    /// Hides / shows elements on the screen to prepare the screen view for a screenshot.
    /// </summary>
    public void PrepareScreenshots()
    {
        logo.SetActive(true);
        playingScore.SetActive(true);
        if (gameMode == GAMEMODE.SHOOTOUT)
            playingScore.GetComponent<TextMeshProUGUI>().text = PlayerPrefs.GetInt(ENT_CONSTANTS.PREFS_HIGH_SCORE_SHOOTOUT).ToString();
        else
            playingScore.GetComponent<TextMeshProUGUI>().text = PlayerPrefs.GetInt(ENT_CONSTANTS.PREFS_HIGH_SCORE_STANDARD).ToString();
        nonPlayingScore.SetActive(false);
        gameModeTextHolder.SetActive(false);
    }

    /// <summary>
    /// Reverses the effects of the PepareScreenshots Method.
    /// </summary>
    public void UnprepareScreenshots()
    {
        logo.SetActive(false);
        playingScore.SetActive(false);
        if (gameMode == GAMEMODE.SHOOTOUT)
            playingScore.GetComponent<TextMeshProUGUI>().text = PlayerPrefs.GetInt(ENT_CONSTANTS.PREFS_HIGH_SCORE_SHOOTOUT).ToString();
        else
            playingScore.GetComponent<TextMeshProUGUI>().text = PlayerPrefs.GetInt(ENT_CONSTANTS.PREFS_HIGH_SCORE_STANDARD).ToString();
        nonPlayingScore.SetActive(true);
        gameModeTextHolder.SetActive(true);
    }

    /// <summary>
    /// Sends command to the backboard to start moving.
    /// </summary>
    public void MoveBackboard()
    {
        backboard.Move(true);
    }

    /// <summary>
    /// Set the Free Money Button interactability to sent in boolean.
    /// </summary>
    /// <param name="interactable"></param>
    public void SetFreeMoneyButtonInteraction(bool interactable)
    {
        freeMoneyButton.interactable = interactable;
    }

    /// <summary>
    /// Sets the loggedIn value to the passed in boolean.
    /// </summary>
    /// <param name="loggedIn"></param>
    public void SetUserLogin(bool loggedIn = false)
    {
        this.loggedIn = loggedIn;
    }
    
    /// <summary>
    /// Returns true if the store is active. Checks if the store screen is obstructing your view.
    /// </summary>
    /// <returns></returns>
    public bool ObstructedByUI()
    {
        return store.activeSelf;
    }

    /// <summary>
    /// Open the Application page on Google Play Store.
    /// </summary>
    public void RateUs()
    {
        Application.OpenURL("http://play.google.com/store/apps/details?id=" + Application.identifier);
    }

    /// <summary>
    /// Takes the price [int] sent to it and then calls add coin in the negative of that price.
    /// </summary>
    /// <param name="price"></param>
    public void SpendCoins(int price)
    {
        AddCoin(-price);
    }

    private void Update()
    {
        adTimer += Time.deltaTime;
    }

    /// <summary>
    /// Add a coin to the coin total. Send in how many coins to add and if a coin object had called this. 
    /// </summary>
    /// <param name="coinsToAd"></param>
    /// <param name="coinObject"></param>
    public void AddCoin(int coinsToAd = 1, EarnACoin coinObject = null)
    {
        coins += coinsToAd;
        UpdateCoinVisually();
        PlayerPrefs.SetInt(ENT_CONSTANTS.PREFS_TRACK_COINS, coins); // Save new coin value to prefs //

        if (coinObject != null)
            RemoveCoin(coinObject); // Remove the coin object from the scene view //

        // Grow our coin holder in corner, visual effect //
        CornerCoin.instance.Grow();
    }

    /// <summary>
    /// Send in an EarnACoin coin, and the object will be removed from active coins and no longer visible.
    /// </summary>
    /// <param name="coinToRemove"></param>
    private void RemoveCoin(EarnACoin coinToRemove)
    {
        activeCoins.Remove(coinToRemove);
        coinToRemove.gameObject.SetActive(false);
    }

    /// <summary>
    /// Takes the price sent in as an int and returns boolean stating if you have enough coins for a purchase.
    /// </summary>
    /// <param name="price"></param>
    /// <returns></returns>
    public bool HasEnoughCoins(int price)
    {
        if (coins >= price)
            return true;
        return false;
    }

    /// <summary>
    /// Collect our 'daily' reward.  Spanws and collects random amount of coins.
    /// </summary>
    public void CollectDaily()
    {
        SpawnAndCollectCoins(UnityEngine.Random.Range(50, 100), dailyButton.position); // Spawn the coins
        DailyRewards.instance.AwardCollected(); // Tell the daily rewards button to reset.
    }

    /// <summary>
    /// When a ball goes through the hoop we want to add a point.
    /// </summary>
    /// <param name="point"></param>
    public void AddPoint(int point = 1)
    {
        // SWISH CHECK //
        if (point > 1)
            perfectsInARow++;
        else
            perfectsInARow = 0;

        // SPAWN COINS //
        var chance = coinChance;
        if (gameMode == GAMEMODE.SHOOTOUT)
            chance = chance / 10;

        // Randomly spawn a coin on the map.  Only spawn one unless perfects are greater than five. Then we spawn
        // any amount of coins between 5 up to the amount of perfects they've had.
        if (UnityEngine.Random.Range(0, 100) > (100 - chance))
        {
            ResetCoins();
            if (perfectsInARow > 5)
            {
                SpawnCoin(UnityEngine.Random.Range(5, perfectsInARow));
            }
            else
                SpawnCoin(1);
        }

        
        // SET THE MULTIPLYER //
        if (perfectsInARow >= 5)
            multiplyer = 4;
        else if (perfectsInARow >= 3)
            multiplyer = 3;
        else if (perfectsInARow >= 2)
        {
            multiplyer = 2;
            BallManager.instance.Electrify(true);
        }
        else
        {
            multiplyer = 1;
            BallManager.instance.Electrify(false);
        }

        background.SetInteger("multiplyer", multiplyer);

        // ADD TO SCORE AND SHOW SCORE //
        score += point * multiplyer;
        playingScoreText.text = score.ToString();
        lastScoreText.text = score.ToString();
        
        // AFTER SCORE EVENTS by GAMEMODE //
        if (gameMode == GAMEMODE.STANDARD)
        {
            BallManager.instance.Graphic(true);
            // if score is greater than a certain ammount we can randomize the balls positions
            BallManager.instance.RandomizeLocation(score);

            // if score is over a certain ammount we can start moving the hoop .. //
            if (score >= 10)
            {
                backboard.Move(true);
            }
            else
            {
                backboard.Move(false);
            }
        }
        else  if (gameMode == GAMEMODE.SHOOTOUT)
        {
            // ADD TIME FOR SHOOTOUT //
            countDownTimer.AddTime(.01f);
        }
    }

    /// <summary>
    /// Spawn any number of coins. This will literally spawn those coins between the rim no matter what.
    /// </summary>
    /// <param name="numberOfCoinsToSpawn"></param>
    private void SpawnCoin(int numberOfCoinsToSpawn)
    {
        for (int i = 0; i < numberOfCoinsToSpawn; i++)
        {
            GameObject spawnedCoin = ObjectPooler.instance.SpawnFromPool("Coins", transform.position, transform.rotation);
            GameObject[] rims = GameObject.FindGameObjectsWithTag("rimh");
            int selected = UnityEngine.Random.Range(0, rims.Length - 1);
            Vector3 lrim = rims[selected].transform.Find("RimLCollider").transform.position;
            Vector3 rrim = rims[selected].transform.Find("RimRCollider").transform.position;
            rrim += Vector3.up;
            spawnedCoin.GetComponent<EarnACoin>().SpawnCoinBetween(lrim, rrim);
            activeCoins.Add(spawnedCoin.GetComponent<EarnACoin>());
        }
    }

    /// <summary>
    /// This will spawn any number of coins at any position on the screen, but collect them all immediately.
    /// </summary>
    /// <param name="numberOfCointsToSpawn"></param>
    /// <param name="spawnPosition"></param>
    public void SpawnAndCollectCoins(int numberOfCointsToSpawn, Vector3 spawnPosition)
    {
        CloseOverlay();  // Hide any overlays so you can see coins moving.
        int maxCoinsToPhysicallySpawn = 50;
        int physicallySpawn = 0;
        int virtuallyCollect = 0;

        // We don't want to overlaod the system with too many objects.  So we only spawn a max set of coins.
        // The others get collected virtually [meaning we don't see them physically show up on the screen.]
        // But any coins up to that max physical coin value will spawn physically and fly to the score holder int the corner.
        if(numberOfCointsToSpawn > maxCoinsToPhysicallySpawn)
        {
            physicallySpawn = maxCoinsToPhysicallySpawn;
            virtuallyCollect = numberOfCointsToSpawn - maxCoinsToPhysicallySpawn;
        }
        else
        {
            physicallySpawn = numberOfCointsToSpawn;
            virtuallyCollect = 0;
        }
        
        int first = 0;
        for (int i = 0; i < physicallySpawn; i++)
        {
            GameObject spawnedCoin = ObjectPooler.instance.SpawnFromPool("Coins", transform.position, transform.rotation);
            Vector3 randomVector = new Vector3(UnityEngine.Random.Range(-0.5f, 0.5f), UnityEngine.Random.Range(-0.5f, 0.5f));
            Vector3 randomVector2 = new Vector3(UnityEngine.Random.Range(-0.5f, 0.5f), UnityEngine.Random.Range(-0.5f, 0.5f));
            Debug.Log(randomVector);
            Debug.Log(randomVector2);
            spawnedCoin.GetComponent<EarnACoin>().SpawnCoinBetween(spawnPosition + randomVector, spawnPosition + randomVector2);
            activeCoins.Add(spawnedCoin.GetComponent<EarnACoin>());
            if(first == 0)
            {
                spawnedCoin.GetComponent<EarnACoin>().PlayAudio();
                spawnedCoin.GetComponent<EarnACoin>().VirtuallyCollect(virtuallyCollect);
            }
                
            spawnedCoin.GetComponent<EarnACoin>().Animate();
            first++;
        }
    }

    /// <summary>
    /// Save the high score based on the game mode's key name. Send in the game modes keyname as the keyname value and the
    /// current new score.
    /// </summary>
    /// <param name="keyName"></param>
    /// <param name="newScore"></param>
    public void SaveHighScore(string keyName, int newScore)
    {
        if (PlayerPrefs.HasKey(keyName))
        {
            if (PlayerPrefs.GetInt(keyName) < newScore)
            {
                PlayerPrefs.SetInt(keyName, newScore);
            }
        }
        else
        {
            PlayerPrefs.SetInt(keyName, newScore);
        }
    }

    /// <summary>
    /// Clear out the visual score. Save the score, and send the score to the respective leaderboard.
    /// We also reset the scene here as well.
    /// </summary>
    public void ClearScore()
    {
        // Save Score //
        if(gameMode == GAMEMODE.STANDARD)
        {
            SaveHighScore(ENT_CONSTANTS.PREFS_HIGH_SCORE_STANDARD, score);
            PlayerPrefs.SetInt(ENT_CONSTANTS.PREFS_LAST_SCORE_STANDARD, score);
            bestScoreText.text = PlayerPrefs.GetInt(ENT_CONSTANTS.PREFS_HIGH_SCORE_STANDARD).ToString();
            if (loggedIn)
                PlayServicesController.instance.SendScoreToLeaderboard(score, ENT_CONSTANTS.LEADERBOARD_STANDARD);
        }else if(gameMode == GAMEMODE.SHOOTOUT)
        {
            SaveHighScore(ENT_CONSTANTS.PREFS_HIGH_SCORE_SHOOTOUT, score);
            PlayerPrefs.SetInt(ENT_CONSTANTS.PREFS_LAST_SCORE_SHOOTOUT, score);
            bestScoreText.text = PlayerPrefs.GetInt(ENT_CONSTANTS.PREFS_HIGH_SCORE_SHOOTOUT).ToString();
            if (loggedIn)
                PlayServicesController.instance.SendScoreToLeaderboard(score, ENT_CONSTANTS.LEADERBOARD_SHOOTOUT);
        }

        // Reset Scene //
        background.SetInteger("multiplyer", 0);
        BallManager.instance.Electrify(false);
        gameModeTextHolder.SetActive(true);
        score = 0;
        playingScoreText.text = score.ToString();
        playingScore.SetActive(false);
        nonPlayingScore.SetActive(true);
        perfectsInARow = 0;
        ResetAllActiveItems();
        backboard.Move(false);
        BallManager.instance.RandomizeLocation(0);
        flingable = false;
        StartCoroutine(Wait());
        if(adTimer > 180)
        {
            AdInter.instance.GameOver();
            adTimer = 0;
        }
    }

    /// <summary>
    /// Waiting a sixth of a second before you can fling again, to prevent accidental flinging.
    /// </summary>
    /// <returns></returns>
    private IEnumerator Wait()
    {
        yield return new WaitForSeconds(.6f);
        flingable = true;
    }

    /// <summary>
    /// Show the leaderboard.
    /// </summary>
    public void ShowLeaderboard()
    {
        if(gameMode == GAMEMODE.STANDARD)
            PlayServicesController.instance.OpenLeaderboard(ENT_CONSTANTS.LEADERBOARD_STANDARD);
        else if(gameMode == GAMEMODE.SHOOTOUT)
            PlayServicesController.instance.OpenLeaderboard(ENT_CONSTANTS.LEADERBOARD_SHOOTOUT);
    }

    /// <summary>
    /// Reset items on screen to defaults.
    /// </summary>
    private void ResetAllActiveItems()
    {
        BallManager.instance.Graphic(true);

        ResetBalls();
        ResetCoins();

        started = false;
    }

    /// <summary>
    /// Reset the balls to an inactive state and clear them from the screen.
    /// </summary>
    private void ResetBalls()
    {
        // Reset all the balls //
        foreach (Ball ball in activeBalls)
        {
            ResetBall(ball);
        }

        // Clear all balls form Active //
        activeBalls.Clear();
    }

    /// <summary>
    /// Reset the coins to an incactive state and clear them from the screen.
    /// </summary>
    private void ResetCoins()
    {
        // Set all coins to inactive //
        foreach (EarnACoin coin in activeCoins)
        {
            if(!coin.TheCoinIsMoving())
                coin.transform.gameObject.SetActive(false);
        }

        // Clear all coins from active //
        activeCoins.Clear();
    }

    /// <summary>
    /// Reset the ball that is passed in.
    /// </summary>
    /// <param name="ball"></param>
    public void ResetBall(Ball ball)
    {
        ball.Reset();
    }

    /// <summary>
    /// Swap through the game modes. And call Load Game Mode once a new gamemode is set.
    /// </summary>
    public void ChangeGameMode()
    {
        switch (gameMode)
        {
            case GAMEMODE.SHOOTOUT:
                gameMode = GAMEMODE.STANDARD;
                break;
            case GAMEMODE.STANDARD:
                gameMode = GAMEMODE.SHOOTOUT;
                break;  
        }
        LoadGameMode();
    }

    /// <summary>
    /// Start the game, depending on which game mode.
    /// </summary>
    public void StartGame()
    {
        lastScoreText.text = score.ToString();
        playingScore.SetActive(true);
        nonPlayingScore.SetActive(false);
        started = true;
        gameModeTextHolder.SetActive(false);

        // Game Mode specefics on START of that game //
        if (gameMode == GAMEMODE.SHOOTOUT)
        {
            countDownTimer.gameObject.SetActive(true);
            countDownTimer.StartCountdown(15f);
        }
    }

    /// <summary>
    /// Add a ball, passed in, to the active balls list.
    /// </summary>
    /// <param name="ball"></param>
    public void AddActiveBall(Ball ball)
    {
        activeBalls.Add(ball);
    }

    /// <summary>
    /// Open the store window.
    /// </summary>
    public void OpenStore()
    {
        overlay.SetActive(true);
        store.SetActive(true);
        customize.SetActive(false);
    }

    /// <summary>
    /// Open the customization window
    /// </summary>
    public void OpenCustomize()
    {
        overlay.SetActive(true);
        store.SetActive(false);
        customize.SetActive(true);
    }

    /// <summary>
    /// Close all overlays.
    /// </summary>
    public void CloseOverlay()
    {
        overlay.SetActive(false);
        store.SetActive(false);
        customize.SetActive(false);
    }

    /// <summary>
    /// Kill the ads and set the preferences to no longer have ads. Ads are dead.
    /// </summary>
    public void KillAds()
    {
        hasAds = false;
        PlayerPrefs.SetInt(ENT_CONSTANTS.PREFS_HAS_ADS, hasAds ? 1 : 0);
        banner.Destroy();
        DisableRemoveAdsButton();
    }

    /// <summary>
    /// Show the ads, re-awaken those ads baby. They are no longer dead.
    /// </summary>
    public void ShowAds()
    {
        hasAds = true;
        PlayerPrefs.SetInt(ENT_CONSTANTS.PREFS_HAS_ADS, hasAds ? 1 : 0);
        banner.Reload();
        DisableRemoveAdsButton();
    }
}
