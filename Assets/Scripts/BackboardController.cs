﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackboardController : MonoBehaviour {
    
    public void Move(bool m)
    {
        GetComponent<Animator>().SetBool("move", m);
    }
}
