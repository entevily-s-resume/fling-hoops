﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public enum SegmentType
{
    BALL_COMMON,
    BALL_RARE,
    BALL_EXTINCT,
    BOARD_COMMON,
    BOARD_RARE,
    BOARD_EXTINCT
}

public class UnlockableSegment : MonoBehaviour {
    public SegmentType segmentType;

    public TextMeshProUGUI segmentTitle;
    public GameObject unlockableGroup;
    public GameObject unlockablePrefab;
    
}
