﻿using System;
using UnityEngine;

public class Ball : MonoBehaviour {
    [SerializeField]
    private AudioClip swish, ballFling;
    [SerializeField]
    private AudioClip[] ballBounce;
    public GameObject electricity;

    private float speed = 12.6f;

    private Rigidbody2D rb;
    private AudioSource audioSoruce;

    private Vector3 startingScale;
    private Vector3 lastPo;
    private Vector3 direction;

    private LayerMask NoColLayer;
    private LayerMask ColLayer;
    
    private bool goingAway = true;
    private bool scoringBall = false;

    private bool clearedScore = false;
    private bool doneGoingAway = false;
    private bool rimmed = false;

    private int ballDownSortingLayer;

    //private float startingDistance;
    //public float StartingDistance { set { startingDistance = value; } }

    private void Awake()
    {
        audioSoruce = GetComponent<AudioSource>();
        rb = GetComponent<Rigidbody2D>();
        startingScale = transform.localScale;
    }

    private void Start()
    {
        audioSoruce.PlayOneShot(ballFling);
        lastPo = transform.position;
        NoColLayer = LayerMask.NameToLayer("ball_nocol");
        ColLayer = LayerMask.NameToLayer("ball_col");
    }

    bool first = false;

    private void Update()
    {
        float distance = Vector3.Distance(transform.position, GameObject.Find("Rim").transform.position);
        rb.AddForce(Vector3.down * distance * speed);

        if (transform.position.y < -50)
        {
            HoopsGM.instance.ResetBall(this);
        }

        if (HoopsGM.instance.gameMode == GAMEMODE.STANDARD && !goingAway && !scoringBall && !clearedScore && transform.position.y < 1)
        {
            clearedScore = true;
            HoopsGM.instance.ClearScore();
        }

        if (!doneGoingAway && rb.velocity.y >= 0)
        {
            goingAway = true;
        }
        else
        {
            doneGoingAway = true;
            goingAway = false;
        }
    }

    private void FixedUpdate()
    {
        if (goingAway)
        {
            // Shrinking //
            transform.localScale = transform.localScale * .9866f;
        }

        if (!goingAway && (gameObject.layer == NoColLayer))
        {
            // Change Layers //
            gameObject.layer = ColLayer;
            GetComponent<SpriteRenderer>().sortingLayerName = "BallDown";
        }
    }
    
    public void OnCollisionEnter2D(Collision2D collision)
    {
        // Hit the Rim //
        if (collision.collider.tag == "rim")
        {
            rimmed = true;
            if (ballBounce.Length > 0)
            {
                audioSoruce.PlayOneShot(ballBounce[UnityEngine.Random.Range(0, ballBounce.Length)]);
                //Debug.Log("Rim Hit");
            }
               
        }
    }

    public bool HasScored()
    {
        return scoringBall;
    }
    
    public void Scored()
    {
        scoringBall = true;
        audioSoruce.PlayOneShot(swish);
    }
    
    public void ApplyToActiveBalls()
    {
        HoopsGM.instance.AddActiveBall(this);
    }

    public void Reset()
    {
        goingAway = true;
        scoringBall = false;
        clearedScore = false;
        doneGoingAway = false;
        rimmed = false;

        gameObject.layer = NoColLayer;
        transform.localScale = startingScale;
        GetComponent<SpriteRenderer>().sortingLayerName = "BallUp";
        rb.velocity = Vector3.zero;

        gameObject.SetActive(false);
    }

    internal void SetGraphic(Sprite sprite)
    {
        GetComponent<SpriteRenderer>().sprite = sprite;
    }

    public bool HitTheRim()
    {
        return rimmed;
    }
}

// Old UPDATE //
/*

*/
