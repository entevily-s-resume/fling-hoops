﻿using System;

[Serializable]
public class UnlockableItem {
    public string id;
    public string name;

    public string graphicName;
    public SegmentType unlockableType;
    public string[] args;
}
