﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CornerCoin : MonoBehaviour {
    internal static CornerCoin instance;

    Vector3 startingScale;

    RectTransform rectShape;

    private void Awake()
    {
        MakeSingleton();
        rectShape = GetComponent<RectTransform>();
        startingScale = rectShape.localScale;
    }

    private void MakeSingleton()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);
    }
    
	
	// Update is called once per frame
	void Update () {
		if(rectShape.localScale != startingScale)
        {
            rectShape.localScale = Vector3.Lerp(rectShape.localScale, startingScale, 100 * Time.deltaTime);
        }
	}

    public void Grow()
    {
        rectShape.localScale += new Vector3(.1f, .1f, .1f);
    }
}
