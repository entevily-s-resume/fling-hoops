public static class ENT_CONSTANTS
{
    public enum AUTHSTATE
    {
        allowed,
        cancelled
    }

    // API KEY / GOOGLE CODES / ETC //
    public const string LEADERBOARD_STANDARD = "CgkIpIau_swOEAIQAg";
    public const string LEADERBOARD_SHOOTOUT = "CgkIpIau_swOEAIQAw";

    // PREFS //
    public const string PREFS_HIGH_SCORE_STANDARD = "COM.ENTUVU.HIGHSCORE.STANDARD";
    public const string PREFS_HIGH_SCORE_SHOOTOUT = "COM.ENTUVU.HIGHSCORE.SHOOTOUT";
    public const string PREFS_LAST_SCORE_STANDARD = "COM.ENTUVU.LASTSCORE.STANDARD";
    public const string PREFS_LAST_SCORE_SHOOTOUT = "COM.ENTUVU.LASTSCORE.SHOOTOUT";
    public const string PREFS_AUTHORIZATION = "COM.ENTUVU.AUTHORIZATION";

    public const string PREFS_HAS_ADS = "COM.ENTUVU.HASADS";

    public const string PREFS_TRACK_COINS = "COM.ENTUVU.COINS";

    // OTHER COSNTANTS //
    public const string STR_GM_SHOOTOUT = "SHOOTOUT";
    public const string STR_GM_STANDARD = "ENDLESS";
}