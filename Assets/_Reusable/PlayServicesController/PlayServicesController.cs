﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;
using GooglePlayGames;

public class PlayServicesController : MonoBehaviour
{
    public static PlayServicesController instance;

    public UnityEvent UserPassLogIn;
    public UnityEvent UserFailedLogIn;

    private void Awake()
    {
        MakeSingleton();
    }

    private void MakeSingleton()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);
    }

    private void Start()
    {
        // Activate the play games platform //
        PlayGamesPlatform.Activate();
        if(!PlayerPrefs.HasKey(ENT_CONSTANTS.PREFS_AUTHORIZATION) || ((PlayerPrefs.HasKey(ENT_CONSTANTS.PREFS_AUTHORIZATION) && PlayerPrefs.GetInt(ENT_CONSTANTS.PREFS_AUTHORIZATION) == 0 )))
            StartCoroutine(WaitAndLogin());
    }

    private IEnumerator WaitAndLogin()
    {
        Debug.Log("Waiting a literal second and logging into Google Play Services.");
        yield return new WaitForSeconds(1);
        LogOnGooglePlay();
    }

    public void LogOnGooglePlay(string leaderboardID = "")
    {
        Debug.Log("Checking if we are authenticated.");
        if (Social.localUser.authenticated)
        {
            Debug.Log("Current user was previously authenticated, nothing to do.");
            PlayerPrefs.SetInt(ENT_CONSTANTS.PREFS_AUTHORIZATION, 0); // SET AUTH TO 0 MEANING THE USER WILLING TO AUTHENTICATE //
            UserPassLogIn.Invoke(); // CALL OUT OUR EVENT - HOOPS GM USER IS LOGGED IN //
        }
        else
        {
            Debug.Log("Current user is not authenticated, trying to authenticate.");
            Social.localUser.Authenticate((bool success) => {
                if (success)
                {
                    Debug.Log("Current user has been authenticated.");
                    PlayerPrefs.SetInt(ENT_CONSTANTS.PREFS_AUTHORIZATION, 0);  // SET AUTH TO 0 MEANING THE USER WILLING TO AUTHENTICATE //
                    UserPassLogIn.Invoke(); // CALL OUT EVENT - HOOPS GM USER IS LOGGED IN //
                    if (!string.IsNullOrEmpty(leaderboardID)) // CHECK IF THE LEADBOARD WAS SENT INT, THEN WE SIGN IN AND LAUNCH THAT LEADERBOARD //
                        OpenLeaderboard(leaderboardID);
                }
                else
                {
                    Debug.Log("Current user has failed to authenticate or authentication has been cancelled.");
                    PlayerPrefs.SetInt(ENT_CONSTANTS.PREFS_AUTHORIZATION, 1); // SET AUTH TO 1 MEANING THE USE RNOT WILLING TO AUTHENTICATE //
                    UserFailedLogIn.Invoke(); // CALL OUT EVENT - HOOPS GM USER IS LOGGED IN FALSE //
                }
            });
        }
    }

    public void LogOffGooglePlay()
    {
        if (Social.localUser.authenticated)
        {
            PlayGamesPlatform.Instance.SignOut();
        }
    }

    public void ToggleGooglePlayLogin()
    {
        if (Social.localUser.authenticated)
            LogOffGooglePlay();
        else
            LogOnGooglePlay();
    }

    public void OpenLeaderboard(string leaderboard)
    {
        // Leaderboards can only be luanched by an authenticated user.  Check if that user has been
        // Authenticated. If so, launch that leaderboard.  If not, send user to log in and then launch the leaderboard.
        if (Social.localUser.authenticated)
        {
            Debug.Log("Authenticated, launching leaderboards.");
            PlayGamesPlatform.Instance.ShowLeaderboardUI(leaderboard);
        }
        else
        {
            Debug.Log("Not Authenticated, Trying to login.");
            LogOnGooglePlay(leaderboard);
        }
    }

    public void SendScoreToLeaderboard(int score, string leaderboard)
    {
        Debug.Log("Trying to report score of " + score + " to leaderboard " + leaderboard);
        // Check if the suer is logged in first //
        if (Social.localUser.authenticated)
        {
            Debug.Log("User is already authenticated.");
            Social.ReportScore(score, leaderboard, (bool succes) => {
                if (succes)
                {
                    Debug.Log("Score was successfully reported");
                }
                else
                {
                    Debug.Log("Scored failed to report.");
                }
            });
            return;
        }
        Debug.Log("Score cannot be reported yet, not authenticated.");
    }

    public void UnlockAchievement(string acheivmentCode)
    {
        PlayGamesPlatform.Instance.UnlockAchievement(acheivmentCode);
    }
}