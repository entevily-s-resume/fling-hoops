﻿using UnityEngine;
using System.IO;
using System;
using System.Collections;

public class NativeShare : MonoBehaviour {

    private bool isProcessing = false;
    private bool isFocus = false;
    private string message;

    public void Share()
    {
        if (!isProcessing)
        {
            // Switch to canvas you want to share //
            HoopsGM.instance.PrepareScreenshots();
            if(HoopsGM.instance.gameMode == GAMEMODE.SHOOTOUT)
            {
                message = "Can you beat my best score in Fling Hoops SHOOTOUT! Check it out at https://play.google.com/store/apps/details?id=com.entuvu.flinghoops";
            }
            else
            {
                message = "I wonder if anyoen can beat my best score in Fling Hoops! Check it out at https://play.google.com/store/apps/details?id=com.entuvu.flinghoops";
            }
            StartCoroutine(ShareScreenshot());
        }
    }

    private IEnumerator ShareScreenshot()
    {
        Debug.Log("Start sharing.");
        isProcessing = true;

        yield return new WaitForEndOfFrame();

        ScreenCapture.CaptureScreenshot("screenshot.png", 2);
        string destination = Path.Combine(Application.persistentDataPath, "screenshot.png");

        yield return new WaitForSecondsRealtime(0.3f);

        if (!Application.isEditor)
        {
            AndroidJavaClass intentClass = new AndroidJavaClass("android.content.Intent");
            AndroidJavaObject intentObject = new AndroidJavaObject("android.content.Intent");
            intentObject.Call<AndroidJavaObject>("setAction", intentClass.GetStatic<string>("ACTION_SEND"));
            AndroidJavaClass uriClass = new AndroidJavaClass("android.net.Uri");
            AndroidJavaObject uriObject = uriClass.CallStatic<AndroidJavaObject>("parse", "file://" + destination);
            intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_STREAM"),
                uriObject);
            intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_TEXT"),
                message);
            intentObject.Call<AndroidJavaObject>("setType", "image/jpeg");
            AndroidJavaClass unity = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
            AndroidJavaObject currentActivity = unity.GetStatic<AndroidJavaObject>("currentActivity");
            AndroidJavaObject chooser = intentClass.CallStatic<AndroidJavaObject>("createChooser",
                intentObject, "Share your hight score!");
            currentActivity.Call("startActivity", chooser);

            yield return new WaitForSecondsRealtime(1);
        }
        else
        {
            Debug.Log("Not Android");
        }

        yield return new WaitUntil(() => isFocus);
        // Hide the canvas
        HoopsGM.instance.UnprepareScreenshots();
        isProcessing = false;
        Debug.Log("Done sharing");
    }



    private void OnApplicationFocus(bool focus)
    {
        isFocus = focus;
    }
}
