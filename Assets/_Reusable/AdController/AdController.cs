﻿using GoogleMobileAds.Api;
using UnityEngine;
using System;
using TMPro;

public class AdController : MonoBehaviour {

    public static AdController instance;

    public RewardBasedVideoAd rewardBasedVideo;

    public TextMeshProUGUI t;

    private int failedCount = 0;

    private void MakeSingleton()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);
    }

    private void Awake()
    {
#if UNITY_ANDROID
        string appId = "ca-app-pub-3591338678881095~1703812539";
#else
        string appId = "unexpected_platform";
#endif
        // Initialize the Google Mobile Ads SDK.
        MobileAds.Initialize(appId);
    }

    private void Start()
    {
        rewardBasedVideo = RewardBasedVideoAd.Instance;

        // Get singleton reward based video ad reference.
        this.rewardBasedVideo = RewardBasedVideoAd.Instance;

        // Called when an ad request has successfully loaded.
        rewardBasedVideo.OnAdLoaded += HandleRewardBasedVideoLoaded;
        // Called when an ad request failed to load.
        rewardBasedVideo.OnAdFailedToLoad += HandleRewardBasedVideoFailedToLoad;

        // Called when an ad is shown.
        rewardBasedVideo.OnAdOpening += HandleRewardBasedVideoOpened;
        // Called when the ad starts to play.
        rewardBasedVideo.OnAdStarted += HandleRewardBasedVideoStarted;
        // Called when the user should be rewarded for watching a video.
        rewardBasedVideo.OnAdRewarded += HandleRewardBasedVideoRewarded;
        // Called when the ad is closed.
        rewardBasedVideo.OnAdClosed += HandleRewardBasedVideoClosed;
        // Called when the ad click caused the user to leave the application.
        rewardBasedVideo.OnAdLeavingApplication += HandleRewardBasedVideoLeftApplication;

        RequestRewardedVideo();
    }

    private void RequestRewardedVideo()
    {
        HoopsGM.instance.SetFreeMoneyButtonInteraction(false);
#if UNITY_ANDROID
        string adUnitId = "ca-app-pub-3591338678881095~1703812539";
        if (HoopsGM.instance.debug)
            adUnitId = "ca-app-pub-3940256099942544/5224354917"; // Test Ad Unit
#else
        string adUnitId = "unexpected_platform";
#endif
        // Create an empty ad request.
        AdRequest request = new AdRequest.Builder().Build();
        // Load the rewarded video ad with the request.
        rewardBasedVideo.LoadAd(request, adUnitId);
    }

    public void HandleRewardBasedVideoLoaded(object sender, EventArgs args)
    {
        HoopsGM.instance.SetFreeMoneyButtonInteraction(true);
        failedCount = 0;

        t.text = "HandleRewardBasedVideoLoaded event received";
    }

    public void HandleRewardBasedVideoFailedToLoad(object sender, AdFailedToLoadEventArgs args)
    {
        t.text = "HandleRewardBasedVideoFailedToLoad event received with message: " + args.Message;

        failedCount++;
        HoopsGM.instance.SetFreeMoneyButtonInteraction(false);
        if (failedCount < 4)
            RequestRewardedVideo();
    }

    public void HandleRewardBasedVideoOpened(object sender, EventArgs args)
    {
        t.text = "HandleRewardBasedVideoOpened event received";
    }

    public void HandleRewardBasedVideoStarted(object sender, EventArgs args)
    {
        t.text = "HandleRewardBasedVideoStarted event received";
    }

    public void HandleRewardBasedVideoClosed(object sender, EventArgs args)
    {
        t.text = "HandleRewardBasedVideoClosed event received";
        RequestRewardedVideo();
    }

    public void HandleRewardBasedVideoRewarded(object sender, Reward args)
    {
        string type = args.Type;
        double amount = args.Amount;
        HoopsGM.instance.AddCoin((int)amount);

        t.text = "HandleRewardBasedVideoRewarded event received for "
                        + amount.ToString() + " " + type;

        RequestRewardedVideo();
    }

    public void HandleRewardBasedVideoLeftApplication(object sender, EventArgs args)
    {
        t.text = "HandleRewardBasedVideoLeftApplication event received";
    }
    
    public void WatchReward()
    {
        if (rewardBasedVideo.IsLoaded())
        {
            rewardBasedVideo.Show();
        }
    }
}