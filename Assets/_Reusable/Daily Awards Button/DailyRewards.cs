﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class DailyRewards : MonoBehaviour {

    [SerializeField]
    private GameObject daily, timer;

    [SerializeField]
    private TextMeshProUGUI timerText;

    DateTime currentDate;
    DateTime oldDate;

    public static DailyRewards instance;

    private int update;
    private int updateWhen = 10;

    [SerializeField]
    private int hoursBetweenMoneys = 6;

    private void Update()
    {
        if (timer.activeSelf)
        {
            update++;

            if(update > updateWhen)
            {
                update = 0;
                TimeSpan timeDifference = GetDifference(DateTime.Now, oldDate);
                TimeSpan minutesBetween = TimeSpan.FromHours(hoursBetweenMoneys);
                TimeSpan timeLeft = minutesBetween - timeDifference;

                timerText.text = timeLeft.ToString().Substring(0, timeLeft.ToString().LastIndexOf("."));
                if (timeLeft < TimeSpan.FromSeconds(0))
                {
                    ShowTimer(false);
                    update = 0;
                }
            }
        }
    }

    private TimeSpan GetDifference(DateTime date, DateTime otherDate)
    {
        TimeSpan diff = date.Subtract(otherDate);
        return diff;
    }

    private void Awake()
    {
        MakeSingleton();

        currentDate = DateTime.Now;
        oldDate = DateTime.Parse("10/10/2010");

        if (PlayerPrefs.HasKey("collected"))
        {
            long temp = Convert.ToInt64(PlayerPrefs.GetString("collected"));

            oldDate = DateTime.FromBinary(temp);
            Debug.Log("old date is : " + oldDate);
        }

        TimeSpan diff = currentDate.Subtract(oldDate);
        Debug.Log("difference is : " + diff);

        if(diff < TimeSpan.FromHours(hoursBetweenMoneys))
        {
            ShowTimer();
        }
        else
        {
            ShowTimer(false);
        }
    }

    private void ShowTimer(bool show = true)
    {
        daily.SetActive(!show);
        timer.SetActive(show);
    }

    private void MakeSingleton()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);
    }

    public void AwardCollected()
    {
        PlayerPrefs.SetString("collected", DateTime.Now.ToBinary().ToString());
        oldDate = DateTime.Now;
        ShowTimer();
    }
}
