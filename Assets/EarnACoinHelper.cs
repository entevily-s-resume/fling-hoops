﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EarnACoinHelper : MonoBehaviour {
    [SerializeField]
    private EarnACoin parent;

    public void AnimationComplete()
    {
        parent.MoveToLocation();
    }
}
