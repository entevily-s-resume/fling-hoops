﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StoreManager : MonoBehaviour {
    public GameObject boardsViewport, ballsViewport;

    public void Show(string viewport)
    {
        switch (viewport)
        {
            case "boards":
                boardsViewport.SetActive(true);
                ballsViewport.SetActive(false);
                break;
            case "balls":
                ballsViewport.SetActive(true);
                boardsViewport.SetActive(false);
                break;
            default:
                ballsViewport.SetActive(true);
                boardsViewport.SetActive(false);
                break;
        }
    }

}
